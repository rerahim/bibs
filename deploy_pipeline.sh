#!/bin/bash

kubectl -s api:8080 exec  kafka-0 -- bash -c "kafka-topics.sh --create --zookeeper zk-0.zk:2181 --replication-factor 1 --partitions 1 --topic message"

kubectl -s api:8080 exec -it hm-0 -- bash -c "echo \"CREATE TABLE SENSOR ( FT UNSIGNED_TIME NOT NULl ,ID UNSIGNED_INT NOT null ,M UNSIGNED_INT CONSTRAINT PK PRIMARY KEY (FT DESC,ID)) SALT_BUCKETS=2,UPDATE_CACHE_FREQUENCY='NEVER'; \" | sqlline.py zk-0.zk "
