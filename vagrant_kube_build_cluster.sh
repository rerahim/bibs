#!/bin/bash

source ./version.sh
cd provision

ansible-playbook -i local-kube-inventory.ini  kube-docker-registry.yml

./kube-docker.sh

ansible-playbook -i local-kube-inventory.ini kube-cluster.yml \
   -e kubelet_version=${kubelet_version} \
   -e kubectl_version=${kubectl_version} \
   -e kubernetes_cni_version=${kubernetes_cni_version} \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version}

sleep 15

sudo rm /var/tmp/addon/*

ansible-playbook -i local-kube-inventory.ini  kube-addon.yml \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version}

ansible-playbook haproxy.yml -i local-kube-inventory.ini

cd ../analytics/docker/

./analytics_build.sh

cd ..
./vagrant_analytic_deployment.sh

cd ../app/docker/
./mean_app_build.sh

cd ../../analytics

kubectl -s api:8080 exec  kafka-0 -- bash -c "kafka-topics.sh --create --zookeeper zk-0.zk:2181 --replication-factor 1 --partitions 1 --topic message"

kubectl -s api:8080 exec -it hm-0 -- bash -c "echo \"CREATE TABLE SENSOR ( FT UNSIGNED_TIME NOT NULl ,ID UNSIGNED_INT NOT null ,M UNSIGNED_INT CONSTRAINT PK PRIMARY KEY (FT DESC,ID)) SALT_BUCKETS=2,UPDATE_CACHE_FREQUENCY='NEVER'; \" | sqlline.py zk-0.zk "

./vagrant_pipeline.sh
