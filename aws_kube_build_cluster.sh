#!/bin/bash
export aws_region=us-east-1
export aws_zone=us-east-1b
export cloud_provider=aws

source ./version.sh
cd provision

ansible-playbook install_pyhton.yml -i aws-kube-inventory.ini

ansible-playbook -i aws-kube-inventory.ini kube-docker-registry.yml 

./kube-docker.sh

ansible-playbook kube-cluster.yml -i aws-kube-inventory.ini \
   -e cloud=aws \
   -e kubelet_version=${kubelet_version} \
   -e kubectl_version=${kubectl_version} \
   -e kubernetes_cni_version=${kubernetes_cni_version} \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version}

sleep 30

ansible-playbook kube-addon.yml -i aws-kube-inventory.ini \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version}

ansible-playbook haproxy-cloud.yml -i aws-kube-inventory.ini

cd ../analytics/docker_aws/

./analytics_build.sh

cd ..
./aws_analytic_deployment.sh

cd ../app/docker/
./mean_app_build.sh

cd ..
./aws_mean_app_deployment.sh

cd ../analytics

kubectl -s api:8080 exec  kafka-0 -- bash -c "kafka-topics.sh --create --zookeeper zk-0.zk:2181 --replication-factor 1 --partitions 1 --topic message"

kubectl -s api:8080 exec -it hm-0 -- bash -c "echo \"CREATE TABLE SENSOR ( FT UNSIGNED_TIME NOT NULl ,ID UNSIGNED_INT NOT null ,M UNSIGNED_INT CONSTRAINT PK PRIMARY KEY (FT DESC,ID)) SALT_BUCKETS=2,UPDATE_CACHE_FREQUENCY='NEVER'; \" | sqlline.py zk-0.zk "

./aws_pipeline.sh


kubectl -s api:8080 port-forward nn-0 50070:50070 &
kubectl -s api:8080 port-forward hm-0 16010:16010 &
kubectl -s api:8080 port-forward sm-0 9080:8080 &
kubectl -s api:8080 port-forward pl-0 4040:4040 &


