export kubelet_version=1.5.2-00
export kubectl_version=1.5.2-00
export kubernetes_cni_version=0.5.1-00
export kube_docker_version=v1.5.2
export kubedns_version=1.9
export weave_kube_version=1.8.2
export kubernetes_dashboard_version=v1.5.1
export prometheus_version=v1.3.1
