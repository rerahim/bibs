#!/bin/bash

ssh 10.0.17.11 sudo docker pull dckreg:5000/zookeeper:3.4.8
ssh 10.0.17.12 sudo docker pull dckreg:5000/zookeeper:3.4.8
ssh 10.0.17.13 sudo docker pull dckreg:5000/zookeeper:3.4.8
ssh 10.0.17.14 sudo docker pull dckreg:5000/zookeeper:3.4.8

ssh 10.0.17.12 sudo docker pull dckreg:5000/kafka:0.10.1.1
ssh 10.0.17.12 sudo docker pull dckreg:5000/kafka:0.10.1.1
ssh 10.0.17.13 sudo docker pull dckreg:5000/kafka:0.10.1.1
ssh 10.0.17.14 sudo docker pull dckreg:5000/kafka:0.10.1.1

ssh 10.0.17.12 sudo docker pull dckreg:5000/hadoop:2.7.3
ssh 10.0.17.13 sudo docker pull dckreg:5000/hadoop:2.7.3
ssh 10.0.17.14 sudo docker pull dckreg:5000/hadoop:2.7.3

ssh 10.0.17.12 sudo docker pull dckreg:5000/hbase:1.2.5
ssh 10.0.17.13 sudo docker pull dckreg:5000/hbase:1.2.5
ssh 10.0.17.14 sudo docker pull dckreg:5000/hbase:1.2.5

ssh 10.0.17.12 sudo docker pull dckreg:5000/spark:2.1.0
ssh 10.0.17.13 sudo docker pull dckreg:5000/spark:2.1.0
ssh 10.0.17.14 sudo docker pull dckreg:5000/spark:2.1.0


