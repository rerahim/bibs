#!/bin/bash

${SPARK_VERSION?"Need to set SPARK_VERSION"}

cat Dockerfile.template | \
sed -e "s/{{ SPARK_VERSION }}/$SPARK_VERSION/" \
> Dockerfile

sudo docker build -t dckreg:5000/pipeline:1.1 .
sudo docker push  dckreg:5000/pipeline:1.1

