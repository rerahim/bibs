
source docker_version.conf

echo ""
echo "building hadoop ........"
ssh dckreg mkdir -p /var/tmp/hadoop
scp hadoop/* dckreg:/var/tmp/hadoop
ssh dckreg "export HADOOP_VERSION=$HADOOP_VERSION;cd /var/tmp/hadoop;./build.sh"
