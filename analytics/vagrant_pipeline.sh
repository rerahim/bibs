#!/bin/bash

source conf/vagrant_analytic_pod.conf

cat deploy/pipeline/pipeline.yml | \
sed -e "s/{{ pv_annotations }}/$PV_ANNOTATIONS/" \
    -e "s/{{ pv_annotations_line }}/$PV_ANNOTATIONS_LINE/" \
    -e "s/{{ zk_list }}/$ZK_LIST/"  \
    -e "s/{{ kafka_broker_list }}/$KAFKA_BROKER_LIST/"  \
| kubectl -s api:8080 create -f -

