 #!/bin/bash
###

kubectl -s api:8080 delete -n analytic StatefulSet zk
kubectl -s api:8080 delete -n analytic svc zk

kubectl -s api:8080 delete -n analytic StatefulSet kafka
kubectl -s api:8080 delete -n analytic svc kafka

kubectl -s api:8080 delete -n analytic StatefulSet nn
kubectl -s api:8080 delete -n analytic svc nn

#kubectl -s api:8080 delete -n analytic StatefulSet dn
#kubectl -s api:8080 delete -n analytic svc dn

kubectl -s api:8080 delete -n analytic StatefulSet hm
kubectl -s api:8080 delete -n analytic svc hm

kubectl -s api:8080 delete -n analytic StatefulSet dn
kubectl -s api:8080 delete -n analytic svc dn

kubectl -s api:8080 delete -n analytic StatefulSet dfsrs
kubectl -s api:8080 delete -n analytic svc dfsrs

kubectl -s api:8080 delete -n analytic StatefulSet sm
kubectl -s api:8080 delete -n analytic svc sm

kubectl -s api:8080 delete -n analytic StatefulSet sw
kubectl -s api:8080 delete -n analytic svc sw

kubectl -s api:8080 delete -n analytic StatefulSet pl
kubectl -s api:8080 delete -n analytic svc pl

kubectl -s api:8080 delete -n analytic Deployment  noderest
kubectl -s api:8080 delete -n analytic svc  noderest

kubectl -s api:8080 delete -n analytic Deployment  zeppelin-deployment
kubectl -s api:8080 delete -n analytic svc  zeppelin

kubectl -s api:8080 delete pv  local-0
kubectl -s api:8080 delete pv  local-1
kubectl -s api:8080 delete pv  local-2
kubectl -s api:8080 delete pv  local-3
kubectl -s api:8080 delete pv  local-4
kubectl -s api:8080 delete pv  local-5
kubectl -s api:8080 delete pv  local-6
kubectl -s api:8080 delete pv  local-7


ssh node1 sudo rm -rf /tmp/local*
ssh node2 sudo rm -rf /tmp/local*
ssh node3 sudo rm -rf /tmp/local*

