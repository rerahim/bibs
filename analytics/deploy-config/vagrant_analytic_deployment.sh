#!/bin/bash

cd configMap

./deploy-config.sh

cd ..

source docker_aws/docker_version.conf
source conf/vagrant_analytic_pod.conf

kubectl -s api:8080 create -f local-pv.yml

./analytic_deployment.sh

