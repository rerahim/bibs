kubectl -s api:8080 create namespace analytic
kubectl -s api:8080 create -n analytic -f hdfs-site.yml
kubectl -s api:8080 create -n analytic -f core-site.yml

kubectl -s api:8080 create -n analytic -f hbase-site.yml
kubectl -s api:8080 create -n analytic -f hbase-env.yml
kubectl -s api:8080 create -n analytic -f kafka.server.properties.yml
kubectl -s api:8080 create -n analytic -f spark-env.yml
