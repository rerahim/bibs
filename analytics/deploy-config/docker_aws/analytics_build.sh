#!/bin/bash

source docker_version.conf

echo ""
echo "building java ........"
ssh dckreg mkdir -p /var/tmp/java
scp java/* dckreg:/var/tmp/java
ssh dckreg "cd /var/tmp/java;./build.sh"

echo ""
echo "building zk ........"
ssh dckreg mkdir -p /var/tmp/zk
scp zk/* dckreg:/var/tmp/zk
ssh dckreg "export ZOOKEEPER_VERSION=$ZOOKEEPER_VERSION;cd /var/tmp/zk;./build.sh"


echo ""
echo "building kafka ........"
ssh dckreg mkdir -p /var/tmp/kafka
scp kafka/* dckreg:/var/tmp/kafka
ssh dckreg "export KAFKA_SCALA_VERSION=$KAFKA_SCALA_VERSION; export KAFKA_VERSION=$KAFKA_VERSION;cd /var/tmp/kafka;./build.sh"

echo ""
echo "building hadoop ........"
ssh dckreg mkdir -p /var/tmp/hadoop
scp hadoop/* dckreg:/var/tmp/hadoop
ssh dckreg "export HADOOP_VERSION=$HADOOP_VERSION;cd /var/tmp/hadoop;./build.sh"

echo ""
echo "building hbase ........"
ssh dckreg mkdir -p /var/tmp/hbase
scp hbase/* dckreg:/var/tmp/hbase
ssh dckreg "export HADOOP_VERSION=$HADOOP_VERSION; export HBASE_VERSION=$HBASE_VERSION;export PHOENIX_VERSION=$PHOENIX_VERSION;cd /var/tmp/hbase;./build.sh"

echo ""
echo "building spark ........"
ssh dckreg mkdir -p /var/tmp/spark
scp spark/* dckreg:/var/tmp/spark
ssh dckreg "export HBASE_VERSION=$HBASE_VERSION; export SPARK_VERSION=$SPARK_VERSION; export SPARK_BIN_VERSION=$SPARK_BIN_VERSION; export SBT_VERSION=$SBT_VERSION; cd /var/tmp/spark;./build.sh"

echo ""
echo "building pipeline ........"
ssh dckreg mkdir -p /var/tmp/pipeline
scp pipeline/* dckreg:/var/tmp/pipeline
ssh dckreg "export export SPARK_VERSION=$SPARK_VERSION; cd /var/tmp/pipeline;./build.sh"

echo ""
echo "building zeppelin ........"
ssh dckreg mkdir -p /var/tmp/zeppelin
scp zeppelin/* dckreg:/var/tmp/zeppelin
ssh dckreg "export SPARK_VERSION=$SPARK_VERSION; export ZEPPELIN_VERSION=$ZEPPELIN_VERSION;cd /var/tmp/zeppelin;./build.sh"

echo ""
echo "building node-rest ........"
ssh dckreg mkdir -p /var/tmp/noderestapi
scp -r noderestapi/* dckreg:/var/tmp/noderestapi
ssh dckreg "cd /var/tmp/noderestapi/;./build.sh"

