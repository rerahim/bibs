FROM dckreg:5000/hadoop:{{ HADOOP_VERSION }}

ENV HBASE_VERSION    {{ HBASE_VERSION }}
ENV HBASE_HOME       /opt/hbase

ENV PHOENIX_HOME   /opt/phoenix
ENV PHOENIX_VERSION {{ PHOENIX_VERSION }}

ENV PATH             $PATH:$HBASE_HOME/bin:$PHOENIX_HOME/bin

RUN DEBIAN_FRONTEND=noninteractive
ADD https://s3.amazonaws.com/pivotili.download/hbase-$HBASE_VERSION-bin.tar.gz  hbase-$HBASE_VERSION-bin.tar.gz
RUN tar -zxf hbase-$HBASE_VERSION-bin.tar.gz && \
    rm hbase-$HBASE_VERSION-bin.tar.gz && \
    mv hbase-$HBASE_VERSION $HBASE_HOME

ADD https://s3.amazonaws.com/pivotili.download/apache-phoenix-$PHOENIX_VERSION-bin.tar.gz  apache-phoenix-$PHOENIX_VERSION-bin.tar.gz

RUN tar -zxf apache-phoenix-$PHOENIX_VERSION-bin.tar.gz && \
    rm apache-phoenix-$PHOENIX_VERSION-bin.tar.gz && \
    mv apache-phoenix-$PHOENIX_VERSION-bin $PHOENIX_HOME

RUN cp $PHOENIX_HOME/phoenix-$PHOENIX_VERSION-server.jar $HBASE_HOME/lib/

RUN useradd -u 9001 -m hbase

RUN chown -R hbase $HBASE_HOME
RUN chown -R hbase $PHOENIX_HOME

CMD mkdir -p /etc/jmx_exporter/
COPY hbase-jmx-exporter.yml /etc/jmx_exporter/

RUN mkdir -p /etc/hbase/hbase-site/
RUN mkdir -p /etc/hbase/hbase-env/

RUN mv /opt/hbase/conf/hbase-site.xml /etc/hbase/hbase-site/hbase-site.xml 
RUN mv /opt/hbase/conf/hbase-env.sh /etc/hbase/hbase-env/hbase-env.sh

RUN ln -s /etc/hbase/hbase-site/hbase-site.xml /opt/hbase/conf/hbase-site.xml
RUN ln -s /etc/hbase/hbase-env/hbase-env.sh /opt/hbase/conf/hbase-env.sh

COPY starthbase.sh $HBASE_HOME/bin/starthbase.sh

