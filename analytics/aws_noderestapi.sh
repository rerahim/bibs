#!/bin/bash

source conf/aws_analytic_pod.conf

cat deploy/noderesapi/noderestapi-dep.yml |  \
sed -e "s/{{ kafka_broker_list }}/$KAFKA_BROKER_LIST/" \
| kubectl -s api:8080 create -f -

cat deploy/noderesapi/noderestapi-svc.yml \
| kubectl -s api:8080 create -f -
