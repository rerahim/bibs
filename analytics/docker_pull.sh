#!/bin/bash

ssh node1 sudo docker pull dckreg:5000/zookeeper:3.4.8
ssh node2 sudo docker pull dckreg:5000/zookeeper:3.4.8
ssh node3 sudo docker pull dckreg:5000/zookeeper:3.4.8

ssh node1 sudo docker pull dckreg:5000/kafka:0.10.1.1
ssh node2 sudo docker pull dckreg:5000/kafka:0.10.1.1
ssh node3 sudo docker pull dckreg:5000/kafka:0.10.1.1

ssh node1 sudo docker pull dckreg:5000/hadoop:2.7.3
ssh node2 sudo docker pull dckreg:5000/hadoop:2.7.3
ssh node3 sudo docker pull dckreg:5000/hadoop:2.7.3

ssh node1 sudo docker pull dckreg:5000/hbase:1.2.5
ssh node2 sudo docker pull dckreg:5000/hbase:1.2.5
ssh node3 sudo docker pull dckreg:5000/hbase:1.2.5

ssh node1 sudo docker pull dckreg:5000/spark:2.1.0
ssh node2 sudo docker pull dckreg:5000/spark:2.1.0
ssh node3 sudo docker pull dckreg:5000/spark:2.1.0


