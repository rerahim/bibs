import string
import random
import time

from random import randint
from kafka import KafkaProducer
from kafka.errors import KafkaError

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):

   return ''.join(random.choice(chars) for _ in range(size))

producer = KafkaProducer(bootstrap_servers=['kafka-1.kafka:9092'])

# Asynchronous by default

c = 0
while ( True ):
  r = randint(20,60)
  st = id_generator(r, "6793YUIO")
  future = producer.send('message', '2-' + st)
  c = c+1
  if c % 100 == 0:
    print "sent : " + str(c)   
    time.sleep(1)
