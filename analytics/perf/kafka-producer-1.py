import string
import random
from random import randint
from kafka import KafkaProducer
from kafka.errors import KafkaError

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):

   return ''.join(random.choice(chars) for _ in range(size))

producer = KafkaProducer(bootstrap_servers=['kafka-0.kafka:9092'])

# Asynchronous by default

while ( True ):
  r = randint(20,60)
  st = id_generator(r, "6793YUIO")
  future = producer.send('message', '1-' + st)

