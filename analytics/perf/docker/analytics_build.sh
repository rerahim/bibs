#!/bin/bash

source docker_version.conf

echo ""
echo "building kafka ........"
ssh dckreg mkdir -p /var/tmp/kafka-perf
scp kafka-perf/* dckreg:/var/tmp/kafka-perf
ssh dckreg "export KAFKA_SCALA_VERSION=$KAFKA_SCALA_VERSION; export KAFKA_VERSION=$KAFKA_VERSION;cd /var/tmp/kafka-perf;./build.sh"

